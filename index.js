const express = require('express');
const bodyParser = require('body-parser');
const Message = require('./models/message');
const User = require('./models/user');
const app = express();

app.use(bodyParser.json());

app.get('/', (req, res) => {
  Message.findAll({ include: ['user']}).then(messages => {
    res.json(messages);
  })
  .catch(err => {
    res.sendStatus(500);
  });
});

app.put('/:messageid', (req, res) => {
  Message.findById(req.params.messageid)
  .then(message => {
    if(req.body.message)
      message.message = req.body.message;

    return message.save();
  }).then(() => {
    res.sendStatus(203);
  }).catch(err => {
    res.sendStatus(500);
  });
});

app.delete('/:messageid', (req, res) => {
  Message.findById(req.params.messageid)
  .then(message => {

    return message.destroy();
  }).then(() => {
    res.sendStatus(204);
  }).catch(err => {
    res.sendStatus(500);
  });
});

app.post('/new', (req, res) => {
  if(!req.body.userId || !req.body.message)
    return res.sendStatus(500);

  Message.create(req.body).then(() => {
    res.sendStatus(201);
  })
  .catch(err => {
    res.sendStatus(500);
  });
});

app.listen(3000, async () => {
  await Message.drop();
  await User.drop();
  await User.sync();
  await Message.sync();
  await User.create({name: 'x', password: 'y'});
});
