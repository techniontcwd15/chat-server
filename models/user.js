const Sequelize = require('sequelize');
const db = require('../db');
const Message = require('./message');

const User = db.define('users', {
  name: Sequelize.STRING,
  password: Sequelize.STRING
});

// User.hasMany(Message);

module.exports = User;
