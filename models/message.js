const Sequelize = require('sequelize');
const db = require('../db');
const User = require('./user');

const Message = db.define('messages', {
  message: Sequelize.TEXT
});

Message.belongsTo(User, {as: 'user', foreignKey: 'userId'});

module.exports = Message;
